CentOS 7-based Docker image with common development and deployment tools.

A.k.a. **the workhorse**.

Updated every weekend (automatic rebuild).

## Pulling the image

The image is hosted on GitLab Container Registry:

```bash
docker pull registry.gitlab.com/allgaeuer.fabian/continuous-integration:master
```

## Development

The Vagrantfile allows you to spin up a CentOS 7 virtual machine with Docker installed. No need to pollute your
development system with additional software.

```bash
git clone git@gitlab.com:allgaeuer.fabian/continuous-integration.git
```

Within the repository:

```bash
vagrant up
vagrant ssh
```

Within the VM:

```bash
docker build /vagrant/
```
