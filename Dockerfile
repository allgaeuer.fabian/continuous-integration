FROM centos:7

ADD files /

RUN yum -y install epel-release && \
    rpm -U https://mirror.webtatic.com/yum/el7/webtatic-release.rpm && \
    curl https://rpm.nodesource.com/setup_11.x | bash - && \

    yum -y update && \
    yum -y install \
        openssh-clients \
        rsync \
        git \
        make \
        gcc-c++ \
        jq \
        nodejs \
        yarn \
        php71w-cli \
        php71w-gd \
        php71w-xml \
        php71w-intl \
        php71w-mbstring \
        java-1.8.0-openjdk-devel \
        which \
        google-cloud-sdk \
        powershell && \

    yum clean all

RUN curl -o composer-setup.php https://getcomposer.org/installer && \

    [ $(curl https://composer.github.io/installer.sig) = \
      $(sha384sum composer-setup.php | cut -c 1-96) ] && \

    php composer-setup.php --install-dir=/usr/bin --filename=composer && \
     rm composer-setup.php

RUN curl -Lo netlify.tar.gz \
    $( \
        curl https://api.github.com/repos/netlify/netlifyctl/releases | \
        jq -r '.[0].assets | .[].browser_download_url' | \
        grep linux \
    ) && \
    tar xf netlify.tar.gz -C /usr/bin && \
        rm netlify.tar.gz

RUN export MAVEN_VERSION=$(curl https://api.github.com/repos/apache/maven/tags | jq -r '.[1].name' | cut -d '-' -f 2) && \
    curl -o maven.tar.gz \
        "https://www-eu.apache.org/dist/maven/maven-3/${MAVEN_VERSION}/binaries/apache-maven-${MAVEN_VERSION}-bin.tar.gz" && \
    tar xf maven.tar.gz -C /opt && \
        rm maven.tar.gz && \
    ln -s /opt/apache-maven-${MAVEN_VERSION}/bin/* /usr/bin
